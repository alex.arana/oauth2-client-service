package com.example.server.service

import com.example.server.ApplicationIntegrationTest
import com.example.server.model.ApplicationData
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import javax.inject.Inject

@ApplicationIntegrationTest
class OAuth2ClientServiceIntegrationTest {
    @Inject
    private lateinit var oAuth2ClientService: OAuth2ClientService

    @Test
    fun `fetchTestData returns data from OAuth2-protected resource`() {
        StepVerifier.create(oAuth2ClientService.fetchTestData())
            .expectNext(ApplicationData("data-element-1"))
            .expectNext(ApplicationData("data-element-2"))
            .verifyComplete()
    }
}
