package com.example.server

import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import java.lang.annotation.Inherited

@Inherited
@SpringBootTest
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
@AutoConfigureWebTestClient(timeout = "PT15M")
@AutoConfigureWireMock(port = 0, stubs = ["classpath:/stubs/*.json"])
@WithMockUser(username = "username", password = "password")
@ActiveProfiles("integration-test")
annotation class ApplicationIntegrationTest
