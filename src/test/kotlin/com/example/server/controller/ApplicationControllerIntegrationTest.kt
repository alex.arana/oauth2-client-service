package com.example.server.controller

import com.example.server.ApplicationIntegrationTest
import com.example.server.model.ApplicationData
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import javax.inject.Inject

@ApplicationIntegrationTest
class ApplicationControllerIntegrationTest {
    @Inject
    private lateinit var webTestClient: WebTestClient

    @Test
    fun `API returns data from OAuth2-protected resource`() {
        val response = webTestClient
            .get()
            .uri("/api/tests/datum")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(ApplicationData::class.java)
            .returnResult()
            .responseBody

        assertThat(response).isNotEmpty.contains(
            ApplicationData("data-element-1"),
            ApplicationData("data-element-2")
        )
    }

    /**
     * Test method for when an invalid request is made.
     */
    @Test
    fun `Test endpoint using an invalid REST spec`() {
        webTestClient.get()
            .uri {
                it.path("/api/tests/foo")
                    .queryParam("message", "bar")
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }
}
