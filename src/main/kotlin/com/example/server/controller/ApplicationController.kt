package com.example.server.controller

import com.example.server.model.ApplicationData
import com.example.server.service.OAuth2ClientService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
@RequestMapping(value = ["/api"])
class ApplicationController(private val oAuth2ClientService: OAuth2ClientService) {

    @GetMapping("/tests/datum")
    fun fetchTestData(): Flux<ApplicationData> {
        return oAuth2ClientService.fetchTestData()
    }
}
