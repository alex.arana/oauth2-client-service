package com.example.server.service

import com.example.server.model.ApplicationData
import reactor.core.publisher.Flux

/**
 * Defines the API of the service used to fetch data from an external OAuth2-protected
 * resource.
 */
interface OAuth2ClientService {

    /**
     * Fetches remote data from an OAuth2-protected resource (mocked using WireMock).
     *
     * Canned response stubs found under `src/test/resources/stubs`.
     */
    fun fetchTestData(): Flux<ApplicationData>
}
