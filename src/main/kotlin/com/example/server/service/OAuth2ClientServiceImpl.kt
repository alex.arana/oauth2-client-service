package com.example.server.service

import com.example.server.model.ApplicationData
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlux
import reactor.core.publisher.Flux
import javax.inject.Named

@Named
class OAuth2ClientServiceImpl(
    private val webClient: WebClient
) : OAuth2ClientService {

    override fun fetchTestData(): Flux<ApplicationData> {
        return webClient
            .get()
            .uri("/api/oauth2/data")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlux()
    }
}
