package com.example.server.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.actuate.autoconfigure.security.reactive.EndpointRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.oauth2.client.AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.InMemoryReactiveOAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientProvider
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory

@EnableWebFlux
@EnableWebFluxSecurity
@SpringBootConfiguration
@ComponentScan
class ApplicationConfiguration {

    /**
     * Enables _Basic_ authentication on all published resources with the following exceptions:
     * * All configured _Spring Boot Actuator_ endpoints
     */
    @Bean
    fun securityWebFilterChain(serverHttpSecurity: ServerHttpSecurity): SecurityWebFilterChain {
        return serverHttpSecurity.authorizeExchange()
            .matchers(EndpointRequest.toAnyEndpoint()).permitAll()
            .anyExchange().authenticated()
            .and().httpBasic()
            .and().csrf().disable()
            .build()
    }

    @Bean
    fun reactiveOAuth2AuthorizedClientProvider(): ReactiveOAuth2AuthorizedClientProvider {
        return ReactiveOAuth2AuthorizedClientProviderBuilder.builder()
            .authorizationCode()
            .refreshToken()
            .clientCredentials()
            .password()
            .build()
    }

    @Bean
    fun reactiveOAuth2AuthorizedClientManager(
        clientRegistrationRepository: ReactiveClientRegistrationRepository,
        authorizedClientProvider: ReactiveOAuth2AuthorizedClientProvider
    ): ReactiveOAuth2AuthorizedClientManager {
        return AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(
            clientRegistrationRepository,
            InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrationRepository)
        ).also {
            it.setAuthorizedClientProvider(authorizedClientProvider)
        }
    }

    @Bean
    fun oAuth2WebClient(
        authorizedClientManager: ReactiveOAuth2AuthorizedClientManager,
        @Value("\${oauth2-client-service.target-url}") targetUrl: String
    ) = WebClient.builder()
        .uriBuilderFactory(DefaultUriBuilderFactory(targetUrl))
        .filter(ServerOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager).also {
            it.setDefaultClientRegistrationId("oauth2-client-id")
        })
        .build()
}
