package com.example.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application

/**
 * Main application entry point.
 */
fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
